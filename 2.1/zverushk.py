import time
import sys
import threading
class Pet:
    def __init__(self):
        self.sytost=10
        self.radost=10
        self.lock=threading.Lock()
    def nakormit(self):
        with self.lock:
            if self.sytost>10:
                print("Зверушка сыта, она не хочет есть.")
            else:
                self.sytost+=2
                self.radost-=1
    def poigrat(self):
        with self.lock:
            self.sytost-=1
            self.radost+=2
    def _update_screen(self):
        print("Сытость: "+"*"*self.sytost)
        print("Радость: "+"*"*self.radost)
    def end(self):
        if self.sytost<=0:
            print("Зверушка голодна. Игра закончилась.")
            sys.exit()
        if self.radost<=0:
            print("Зверушке слишком грустно. Игра закончилась.")
            sys.exit()
def igra(zverushka):
    while zverushka.sytost>0 and zverushka.radost>0:
        time.sleep(3)
        with zverushka.lock:
            zverushka.sytost-=1
            zverushka.radost-=2
            zverushka._update_screen()
            print("Выберите действие:")
            print("1.Накормить")
            print("2.Поиграть")
            print("3.Выйти")
            if zverushka.sytost <= 0:
                print("Зверушка голодна. Игра закончилась.")
                sys.exit()
            if zverushka.radost <= 0:
                print("Зверушке слишком грустно. Игра закончилась.")
                sys.exit()
def main():
    zverushka=Pet()
    thread=threading.Thread(target=igra, args=(zverushka,), daemon=True)
    thread.start()
    try:
        while zverushka.sytost > 0 and zverushka.radost > 0:
            zverushka._update_screen()
            print("Выберите действие:")
            print("1.Накормить")
            print("2.Поиграть")
            print("3.Выйти")
            choice=input()
            if choice=="1":
                zverushka.nakormit()
                zverushka.end()
            elif choice=="2":
                zverushka.poigrat()
                zverushka.end()
            elif choice=="3":
                sys.exit()
            else:
                print("Введите корректный выбор!")
            if zverushka.sytost <= 0:
                print("Зверушка голодна. Игра закончилась.")
                sys.exit()
            if zverushka.radost <= 0:
                print("Зверушке слишком грустно. Игра закончилась.")
                sys.exit()
    except KeyboardInterrupt:
        print("Игра закончилась.")
if __name__=="__main__":
    main()
