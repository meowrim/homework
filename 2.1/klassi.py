class My_list(list):
    def __sub__(self,second_list):
        rez=My_list()
        for chislo in range(len(self)):
            try:
                rez.append(self[chislo]-second_list[chislo])
            except IndexError:
                rez.append(self[chislo])
        return rez
    def __truediv__(self, divisor):
        rez=My_list()
        for chislo in self:
            try:
                rez.append(chislo/divisor)
            except ZeroDivisionError:
                return "На ноль делить нельзя!"
        return rez
    def __str__(self):
        rez=super().__str__()
        return f"Результат: {rez}"
l1=My_list([1,2,3])
l2=My_list([3,4,5])
l3=l2-l1
l4=l1-l2
print(l1/5)
print(l3)
print(l4)
