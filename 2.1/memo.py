import pygame
import random
import sys
import time

class Card:
    def __init__(self, value, rect):
        self.value = value
        self.rect = rect
        self.is_open = False
        self.pre_open = True
        self.image = pygame.image.load(f"cards/{value}.png")
        self.card_back = pygame.image.load("cards/card_back.png")
        self.open_timer = pygame.time.get_ticks() + 3000
        self.elapsed_time = 0

    def open_card(self, current_time):
        self.is_open = True
        self.pre_open = False
        self.open_timer = current_time + 2000
        self.elapsed_time = 0

    def close_card(self):
        self.is_open = False
        self.pre_open = False

    def is_open_expired(self, current_time):
        return self.pre_open and current_time >= self.open_timer

class MemoGame:
    def __init__(self, width, height, num_cards):
        pygame.init()

        self.width = width
        self.height = height
        self.num_cards = num_cards

        self.screen = pygame.display.set_mode((width, height))
        pygame.display.set_caption("Memo Game")
        self.clock = pygame.time.Clock()

        self.card_width, self.card_height = 156, 160
        self.gap = 10

        self.selected_card = None
        self.matched = []

        self.reset_game()

        self.background = pygame.image.load("cards/background.png")
        self.background = pygame.transform.scale(self.background, (width, height))

        pygame.mixer.init()
        self.bg_music = pygame.mixer.Sound("cards/fon.mp3")
        self.click_sound = pygame.mixer.Sound("cards/click_sound.mp3")
        self.match_sound = pygame.mixer.Sound("cards/match_sound.mp3")
        self.win_sound = pygame.mixer.Sound("cards/win_sound.mp3")

    def play_background_music(self):
        self.bg_music.play(-1)

    def calculate_card_position(self, i):
        row = i // 6
        col = i % 6
        x = self.width // 2 - (3 * (self.card_width + self.gap)) + col * (self.card_width + self.gap)
        y = self.height // 2 - (2 * (self.card_height + self.gap)) + row * (self.card_height + self.gap)
        return pygame.Rect(x, y, self.card_width, self.card_height)

    def reset_game(self):
        self.cards = [f"card_{i}" for i in range(self.num_cards // 2)] * 2
        random.shuffle(self.cards)

        self.card_objects = [Card(self.cards[i], self.calculate_card_position(i)) for i in range(self.num_cards)]

        self.matched = [False] * self.num_cards
        self.game_active = False

    def draw_game_screen(self):
        self.screen.blit(self.background, (0, 0))
        current_time = pygame.time.get_ticks()

        for i, card in enumerate(self.card_objects):
            if not self.matched[i]:
                if card.is_open or card.is_open_expired(current_time):
                    self.screen.blit(card.image if card.is_open else card.card_back, card.rect)
                elif card.pre_open:
                    self.screen.blit(card.image, card.rect)
                elif not card.is_open and not card.pre_open:
                    elapsed_time = current_time - (
                            card.open_timer - 2000)
                    if 0 <= elapsed_time < 2000:
                        self.screen.blit(card.image, card.rect)
                    else:
                        self.screen.blit(card.card_back, card.rect)
        pygame.display.flip()

    def start_game(self):
        self.game_active = True
        random.shuffle(self.card_objects)
        self.matched = [False] * self.num_cards

        current_time = pygame.time.get_ticks()
        for card in self.card_objects:
            card.pre_open = True
            card.open_timer = current_time + 3000

    def open_card(self, card, current_time):
        card.open_card(current_time)
        self.click_sound.play()

    def check_pair(self, card1, card2):
        if card1.value == card2.value and card1 != card2 and not self.matched[self.card_objects.index(card1)] and not self.matched[self.card_objects.index(card2)]:
            self.matched[self.card_objects.index(card1)] = True
            self.matched[self.card_objects.index(card2)] = True
            self.match_sound.play()

    def close_unmatched_cards(self):
        for i, card in enumerate(self.card_objects):
            if not self.matched[i]:
                card.close_card()

    def end_game(self):
        font = pygame.font.Font(None, 72)
        text = font.render("You Win!", True, (0, 0, 0))
        self.screen.blit(text, (self.width // 2 - text.get_width() // 2, self.height // 2 - text.get_height() // 2))
        pygame.display.flip()
        self.win_sound.play()
        time.sleep(2)
        self.game_active = False

    def resume_game(self):
        if any(self.matched):
            self.game_active = True
            current_time = pygame.time.get_ticks()
            for card in self.card_objects:
                if card.pre_open:
                    card.open_timer = current_time + 3000 - card.elapsed_time

    def pause_game(self):
        self.game_active = False

    def restart_game(self):
        self.reset_game()
        self.start_game()

    def run(self):
        self.play_background_music()
        running = True
        while running:
            current_time = pygame.time.get_ticks()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        self.pause_game()

                if event.type == pygame.MOUSEBUTTONDOWN and self.game_active:
                    for card in self.card_objects:
                        if card.rect.collidepoint(event.pos) and not card.is_open and not self.matched[self.card_objects.index(card)]:
                            if self.selected_card is None:
                                self.selected_card = card
                                self.open_card(self.selected_card, current_time)
                            else:
                                self.open_card(card, current_time)
                                self.draw_game_screen()
                                pygame.display.flip()
                                pygame.time.delay(500)
                                self.check_pair(self.selected_card, card)
                                self.close_unmatched_cards()
                                self.selected_card = None

                if event.type == pygame.MOUSEBUTTONDOWN and not self.game_active:
                    if self.height // 2.7 <= event.pos[1] <= self.height // 2.7 + 36:
                        for card in self.card_objects:
                            card.pre_open = False
                        self.restart_game()
                    elif self.height // 2.7 <= event.pos[1] <= self.height // 2.7 + 186:
                        self.resume_game()
                    elif self.height // 2.7 <= event.pos[1] <= 3 * self.height // 2.7 + 186:
                        running = False

            if self.game_active:
                self.draw_game_screen()

                if all(self.matched):
                    self.end_game()

            else:
                self.screen.blit(self.background, (0, 0))
                font = pygame.font.Font(None, 36)
                text_new_game = font.render("New Game", True, (0, 0, 0))
                text_resume_game = font.render("Resume Game", True, (0, 0, 0))
                text_exit = font.render("Exit", True, (0, 0, 0))

                vertical_spacing = 100
                self.screen.blit(text_new_game, (self.width // 2 - text_new_game.get_width() // 2, self.height // 2.7))
                self.screen.blit(text_resume_game,
                                 (self.width // 2 - text_resume_game.get_width() // 2,
                                  self.height // 2.7 + vertical_spacing))
                self.screen.blit(text_exit, (
                self.width // 2 - text_exit.get_width() // 2, self.height // 2.7 + 2 * vertical_spacing))

                pygame.display.flip()

            self.clock.tick(30)

        pygame.quit()
        sys.exit()

memo_game = MemoGame(1920, 1080, 24)
memo_game.run()
