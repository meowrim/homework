import pygame
import math
width=800
height=700
black=(0, 0, 0)
white=(255, 255, 255)
yellow=(255, 255, 0)
red=(255, 0, 0)
green=(0, 255, 0)
blue=(0, 0, 255)
class Planet:
    def __init__(self, name, radius, distance, speed, color, image_path, real_size, real_distance): #Инициализация объекта класса.
        self.name=name
        self.radius=radius
        self.distance=distance
        self.speed=speed
        self.color=color
        self.angle=0
        self.image_path=image_path
        self.image=None
        self.earth_days=0
        self.real_size=real_size
        self.real_distance=real_distance

    def update(self): #Обновление состояния планеты.
        self.angle+=self.speed
        self.earth_days+=0.3

    def get_polar_position(self): #Получение полярных координат для планеты - x и y координаты в зависимости от угла поворота и расстояния от Солнца.
        x=width//2+math.cos(math.radians(self.angle))*self.distance #Т.косинусов и т.синусов
        y=height//2+math.sin(math.radians(self.angle))*self.distance
        return x, y

    def display_info(self, screen, mouse_pos): #Отображение информации о планете.
        mouse_x, mouse_y=mouse_pos
        distance_to_center=math.sqrt((mouse_x-width//2)**2+(mouse_y-height//2)**2) #Т.Пифагора, чтобы вычислить расстояние от текущего положения мыши до центра экрана.
        if self.distance-10<=distance_to_center<=self.distance+10:
            x, y=self.get_polar_position()
            font=pygame.font.Font(None, 20)
            info_text=f"{self.name}: Размер - {self.real_size}, Расстояние - {self.real_distance} млн.км., Угол - {self.angle}"
            text=font.render(info_text, True, white)
            screen.blit(text, (x,y+self.radius+5))

    def draw(self, screen): #Отрисовка планеты.
        pygame.draw.circle(screen, white, (width// 2, height//2), self.distance, 1)

        x=width//2+math.cos(math.radians(self.angle))*self.distance-self.radius
        y=height//2+math.sin(math.radians(self.angle))*self.distance-self.radius
        if self.image is None:
            self.image=pygame.image.load(self.image_path)
        scaled_image=pygame.transform.scale(self.image, (self.radius*2, self.radius*2))
        screen.blit(scaled_image,(x, y))

        font=pygame.font.Font(None, 20)
        text=font.render(self.name, True, white)
        screen.blit(text, (x, y-20))

        if self.name=="Earth":
            font=pygame.font.Font(None, 20)
            text=font.render("Earth Days: "+str(round(self.earth_days)), True, white)
            screen.blit(text, (10, 10))

pygame.init()
screen=pygame.display.set_mode((width, height))

sun=Planet("Sun", 50, 0, 0, yellow, "image_path/sun.png","1 392 684 км.",0)
mercury=Planet("Mercury", 10, 100, 1, red, "image_path/mercury.png","4 880 км.",58)
venus=Planet("Venus", 20, 200, 0.5, green, "image_path/venus.png","12 100 км.",108)
earth=Planet("Earth", 30, 300, 0.3, blue, "image_path/earth.png","12 742 км.",150)
mars=Planet("Mars", 18, 400, 0.2, red, "image_path/mars.png","6 779 км.",228)
jupiter=Planet("Jupiter", 70, 500, 0.1, yellow, "image_path/jupiter.png","139 822 км.",778)
saturn=Planet("Saturn", 60, 600, 0.08, green, "image_path/saturn.png","116 460 км.",1429)
uranus=Planet("Uranus", 40, 700, 0.06, blue, "image_path/uranus.png","50 724 км.", 2875)
neptune=Planet("Neptune", 35, 800, 0.04, blue, "image_path/neptune.png","49 244 км.", 4497)
planets=[sun, mercury, venus, earth, mars, jupiter, saturn, uranus, neptune]

running=True
clock=pygame.time.Clock()
scale_factor=1.0
scale_step=0.1

while running:
    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            running=False
        elif event.type==pygame.KEYDOWN: #Масштабирование экрана.
            if event.key==pygame.K_PLUS or event.key==pygame.K_KP_PLUS:
                scale_factor+=scale_step
                for planet in planets:
                    planet.radius*=scale_factor
                    planet.distance*=scale_factor
            elif event.key==pygame.K_MINUS or event.key==pygame.K_KP_MINUS:
                scale_factor-=scale_step
                for planet in planets:
                    planet.radius*=scale_factor
                    planet.distance*=scale_factor
    screen.fill(black)
    mouse_position=pygame.mouse.get_pos()
    for planet in planets:
        planet.update()
        planet.draw(screen)
        planet.display_info(screen, mouse_position)
    pygame.display.flip()
    clock.tick(60)
pygame.quit()