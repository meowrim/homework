import time
import tkinter as tk
import random
def task_1():
    print("Вы выбрали задачу 1.")
    a=int(input("Введите первое число: "))
    b=int(input("Введите второе число: "))
    c=int(input("Введите третье число: "))
    a,b,c=c,a,b
    print(a,b,c)
    main()
def task_2_1():
    print("Вы выбрали задачу 2_1.")
    try:
        a=int(input("Введите первое число: "))
        b=int(input("Введите второе число: "))
        c=a+b
        print(c)
    except ValueError:
        print("Введите число!")
        task_2_1()
    main()
def task_2_2():
    print("Вы выбрали задачу 2_2.")
    s=0
    try:
        a=int(input("Количество чисел для суммы: "))
        for i in range(a):
            c=int(input("Введите число: "))
            s+=c
        print(s)
    except ValueError:
        print("Введите число!")
        task_2_2()
    main()
def task_3_1():
    print("Вы выбрали задачу 3_1.")
    x=int(input("Введите число от 0 до 100: "))
    if x>100 or x<0:
        print("Число не входит в диапазон.")
        task_3_1()
    print(x**5)
    main()
def task_3_2():
    print("Вы выбрали задачу 3_2.")
    x=int(input("Введите число от 0 до 100: "))
    if x>100 or x<0:
        print("Число не входит в диапазон.")
        task_3_2()
    print(pow(x,5)) #Сразу оптимизация, без x*x*x*x*x
    main()
def task_4():
    print("Вы выбрали задачу 4.")
    a=0
    b=1
    num=int(input("Введите число от 0 до 250: "))
    if num>250 or num<0:
        print("Число не входит в диапазон.")
        task_4()
    while b<num:
        temp=b
        b=a+b
        a=temp
    if b==num:
        print("Число принадлежит числам Фибонначи.")
    else:
        print("Число не принадлежит числам Фибоначчи.")
    main()
def task_5():
    print("Вы выбрали задачу 5.")
    month=int(input("Введите номер месяца: "))
    if month in [1, 2, 12]:
        print("Зима")
    elif month in [3, 4, 5]:
        print("Весна")
    elif month in [6, 7, 8]:
        print("Лето")
    elif month in [9, 10, 11]:
        print("Осень")
    else:
        print("Неверный месяц")
    main()
def task_6():
    print("Вы выбрали задачу 6.")
    even_count=0
    odd_count=0
    even_sum=0
    odd_sum=0
    n=int(input("Введите число: "))
    for i in range(1, n+1):
        if i%2==0:
            even_count+=1
            even_sum+=i
        else:
            odd_count+=1
            odd_sum+=i
    print(even_sum, even_count, odd_sum, odd_count)
    main()
def task_7():
    print("Вы выбрали задачу 7.")
    divisors=[]
    n=int(input("Введите число меньше 250: "))
    if n>250:
        print("Число должно быть меньше 250.")
        task_7()
    for i in range(1, n+1):
        if n%i==0:
            divisors.append(i)
        print(f"{i} - количество делителей: {len(divisors)}")
    main()
def task_8():
    print("Вы выбрали задачу 8.")
    n=int(input("Введите начало интервала: "))
    m=int(input("Введите конец интервала: "))
    for a in range(n, m+1):
        for b in range(a, m+1):
            for c in range(b, m+1):
                if a**2+b**2==c**2:
                    print(a,b,c)
    main()
def task_9():
    print("Вы выбрали задачу 9.")
    def check(num):
        for digit in str(num):
            if digit=='0' or num%int(digit)!=0:
                return False
        return True
    def find(n, m):
        divisible_numbers=[]
        for num in range(n, m+1):
            if check(num):
                divisible_numbers.append(num)
        return divisible_numbers
    n=int(input("Введите начало интервала: "))
    m=int(input("Введите конец интервала: "))
    result=find(n, m)
    print("Числа, которые делятся на каждую из своих цифр:")
    print(result)
    main()
def task_10():
    print("Вы выбрали задачу 10.")
    def find(n):
        perfect_numbers=[]
        num=1
        while len(perfect_numbers)<n:
            divisors_sum=sum([i for i in range(1, num) if num%i==0])
            if divisors_sum==num:
                perfect_numbers.append(num)
            num+=1
        return perfect_numbers
    n=int(input("Введите количество совершенных чисел(число должно быть больше 5): "))
    if n<5:
        print("Число должно быть больше 5!")
        task_10()
    result=find(n)
    print("Первые", n, "совершенных чисел:")
    print(result)
    main()
def task_11():
    print("Вы выбрали задачу 11.")
    sp=int(input("Введите номер способа от 1 до 3: "))
    if sp==1:
        array=[1, 2, 3, 4, 5]
        print("Последний элемент (способ 1):", array[-1])
        start_time=time.time()
        end_time=time.time()
        print("Время выполнения способа 1:", end_time - start_time)
    if sp==2:
        array=[1, 2, 3, 4, 5]
        print("Последний элемент (способ 2):", array.pop())
        start_time=time.time()
        end_time=time.time()
        print("Время выполнения способа 2:", end_time - start_time)
    if sp==3:
        array = [1, 2, 3, 4, 5]
        print("Последний элемент (способ 3):", array[len(array) - 1])
        start_time=time.time()
        end_time=time.time()
        print("Время выполнения способа 3:", end_time - start_time)
    if sp not in [1, 2, 3]:
        print("Способов 3!")
        task_11()
    main()
def task_12():
    print("Вы выбрали задачу 12.")
    def reverse(array):
        return array[::-1]
    array=[1, 2, 3, 4, 5]
    print(array)
    reversed_array=reverse(array)
    print(f"Массив в обратном порядке: {reversed_array}")
    main()
def task_13():
    print("Вы выбрали задачу 13.")
    def recursive_sum(array, index):
        if index==len(array)-1:
            return array[index]
        return array[index]+recursive_sum(array, index+1)
    array=[6, 7, 8, 9, 10]
    print(array)
    result=recursive_sum(array, 0)
    print(f"Сумма элементов массива: {result}")
    main()
def task_14_1():
    print("Вы выбрали задачу 14_1.")
    def convert():
        rub=int(entry.get())
        rate=0.010309
        usd=round(rub*rate, 2)
        result_label.configure(text=f"{rub} руб. = {usd} $")
    window=tk.Tk()
    window.title("Конвертер рублей в доллары")
    window.geometry("300x200")
    label=tk.Label(window, text="Сумма в рублях:")
    label.pack()
    entry=tk.Entry(window)
    entry.pack()
    button=tk.Button(window, text="Конвертировать", command=convert)
    button.pack()
    result_label=tk.Label(window, text="")
    result_label.pack()
    window.mainloop()
    main()
def task_14_2():
    print("Вы выбрали задачу 14_2.")
    def convert_rubles_to_dollars():
        rub=int(entry.get())
        rate=0.010309
        usd=round(rub*rate, 2)
        result_label.configure(text=f"{rub} руб. = {usd} $")
    def convert_dollars_to_rubles():
        dollars=int(entry1.get())
        exchange_rate=97
        rubles=dollars*exchange_rate
        result_label1.configure(text=f"{dollars} $ = {rubles} руб.")
    window=tk.Tk()
    window.title("Конвертер денег")
    window.geometry("300x200")
    label=tk.Label(window, text="Сумма в рублях:")
    label.pack()
    entry=tk.Entry(window)
    entry.pack()
    button=tk.Button(window, text="Конвертировать", command=convert_rubles_to_dollars)
    button.pack()
    result_label=tk.Label(window, text="")
    result_label.pack()
    label1=tk.Label(window, text="Сумма в долларах:")
    label1.pack()
    entry1=tk.Entry(window)
    entry1.pack()
    button1=tk.Button(window, text="Конвертировать", command=convert_dollars_to_rubles)
    button1.pack()
    result_label1=tk.Label(window, text="")
    result_label1.pack()
    window.mainloop()
    main()
def task_15():
    print("Вы выбрали задачу 15.")
    def table(n, m):
        if n<5 or m<5 or n>20 or m>20:
            print("Размеры таблицы должны быть от 5 до 20")
            task_15()
        for i in range(1, n+1):
            for j in range(1, m+1):
                print(f"{i * j:3}", end=" ")
            print()
    n=int(input("Введите количество строк: "))
    m=int(input("Введите количество столбцов: "))
    table(n, m)
    main()
def task_16():
    print("Вы выбрали задачу 16.")
    field_size=10
    field=[['-' for _ in range(field_size)] for _ in range(field_size)]
    count_ship=7
    ships=[]
    for _ in range(count_ship):
        x=random.randint(0, field_size-1)
        y=random.randint(0, field_size-1)
        ships.append((x, y))
    for ship in ships:
        x,y=ship
        field[x][y]='X'
    for row in field:
        print(' '.join(row))
    main()
def main():
    try:
        num = int(input("Введите номер (если у задачи два номера, то разделитель нижнее подчеркивание): "))
        if num == 1:
            task_1()
        elif num == 2.1 or num == 2_1:
            task_2_1()
        elif num == 2.2 or num == 2_2:
            task_2_2()
        elif num == 3.1 or num == 3_1:
            task_3_1()
        elif num == 3.2 or num == 3_2:
            task_3_2()
        elif num == 4:
            task_4()
        if num == 5:
            task_5()
        elif num == 6:
            task_6()
        elif num == 7:
            task_7()
        elif num == 8:
            task_8()
        elif num == 9:
            task_9()
        elif num == 10:
            task_10()
        elif num == 11:
            task_11()
        elif num == 12:
            task_12()
        elif num == 13:
            task_13()
        elif num==14_1:
            task_14_1()
        elif num==14_2:
            task_14_2()
        elif num==15:
            task_15()
        elif num==16:
            task_16()
        else:
            print("Некорректный ввод. Введите правильный номер задачи. (просто число)")
            main()
    except ValueError:
        print("Некорректный ввод. Введите правильный номер задачи. (просто число)")
        main()
main()