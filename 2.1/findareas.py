import math
class Point:
    def __init__(self, x, y):
        self.x=x
        self.y=y
class Triangle:
    def __init__(self, p1, p2, p3):
        self.points=[p1, p2, p3]
    def virozhden(self):
        return ((self.points[0].x*(self.points[1].y-self.points[2].y))+(self.points[1].x*(self.points[2].y-self.points[0].y))+
                       (self.points[2].x*(self.points[0].y-self.points[1].y)))==0
    def area(self):
        return 0.5*abs((self.points[0].x*(self.points[1].y-self.points[2].y))+(self.points[1].x*(self.points[2].y-self.points[0].y))+
            (self.points[2].x*(self.points[0].y-self.points[1].y)))
    def __eq__(self, other):
        return self.area()==other.area()
    def __lt__(self, other):
        return self.area()<other.area()
    def __le__(self, other):
        return self.area()<=other.area()
    def __gt__(self, other):
        return self.area()>other.area()
    def __ge__(self, other):
        return self.area()>=other.area()
    def __ne__(self, other):
        return self.area()!=other.area()

def read_points(file_path):
    with open(file_path, 'r') as file:
        content=file.read()
        points_str=content[1:-1].split('][')
        points=[Point(*map(int, point.split(','))) for point in points_str]
        return points

def min_max_areas(points):
    triangles=[Triangle(points[i], points[j], points[k]) for i in range(len(points)) for j in
                 range(i+1, len(points)) for k in range(j+1, len(points))]
    valid_triangles=[triangle for triangle in triangles if not triangle.virozhden()]
    if not valid_triangles:
        return None, None
    min_triangle=min(valid_triangles)
    max_triangle=max(valid_triangles)
    return min_triangle, max_triangle

file_path='plist.txt'
points=read_points(file_path)
min_triangle, max_triangle=min_max_areas(points)
if min_triangle and max_triangle:
    print(f"Треугольник с минимальной площадью: {min_triangle.area()}")
    print(f"Tреугольник с максимальной площадью: {max_triangle.area()}")
else:
    print("Нет треугольников с площадью больше 0.")

