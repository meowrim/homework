import pygame
pygame.init()
pygame.mixer.init()
scr=pygame.display.set_mode((300, 300))
scr.fill((255,255,255))
rect = pygame.Rect(10,10,10,10)
scr_rect=scr.get_rect()
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            print("Вы нажали клавишу quit")
            running = False
        if event.type==pygame.KEYDOWN:
            print("Вы нажали клавишу", pygame.key.name(event.key))
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT or event.key == pygame.K_a:
                rect.move_ip(-30, 0)
            elif event.key == pygame.K_RIGHT or event.key == pygame.K_d:
                rect.move_ip(30, 0)
            elif event.key == pygame.K_UP or event.key == pygame.K_w:
                rect.move_ip(0, -30)
            elif event.key == pygame.K_DOWN or event.key == pygame.K_s:
                rect.move_ip(0, 30)
        rect.clamp_ip(scr_rect)
        scr.fill((255, 255, 255))
        pygame.draw.rect(scr, (0, 0, 255), rect, 0)
        pygame.display.flip()