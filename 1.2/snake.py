import pygame
import random
from tkinter import *

red=(255,0,0)
white=(255,255,255)
w=500
h=470
pygame.init()

pygame.display.set_caption('Snake')
scr=pygame.display.set_mode((w, h))
la=pygame.sprite.Sprite()
la.image=pygame.image.load("1562688808.png")
la.rect=la.image.get_rect(bottomright=(w,h))
scr.blit(la.image,la.rect)
pygame.display.update()

snake_position = [100, 50]
fruit_position = [random.randrange(1, (w// 10)) * 10, random.randrange(1, (h// 10)) * 10]
fruit_spawn = True
direction=""
score = 0

main_menu = Menu()
main_menu.add_cascade(label="File")
main_menu.add_cascade(label="Edit")
main_menu.add_cascade(label="View")

def show_score(color, font, size):
    score_font=pygame.font.SysFont(font, size)
    score_surface=score_font.render('Score : ' + str(score), True, color)
    score_rect=score_surface.get_rect()
    scr.blit(score_surface, score_rect)

def game_over():
    my_font=pygame.font.SysFont('Arial', 50)
    game_over_surface=my_font.render('Your Score: ' + str(score), True, red)
    game_over_rect=game_over_surface.get_rect()
    game_over_rect.midtop = (w/2, h/4)
    scr.blit(game_over_surface, game_over_rect)
    pygame.display.flip()
    pygame.quit()
    quit()

snake=pygame.sprite.Sprite()
snake.image = pygame.image.load("body.jpg")
snake.image = pygame.transform.scale(snake.image, (13, 13))
snake.rect = snake.image.get_rect()
while True:
    scr.blit(la.image,la.rect)
    snake_position=[100, 100]
    for event in pygame.event.get():
        if event.type==pygame.KEYDOWN:
            if event.key==pygame.K_UP and direction!='DOWN':
                direction='UP'
            if event.key==pygame.K_DOWN and direction!='UP':
                direction='DOWN'
            if event.key==pygame.K_LEFT and direction!='RIGHT':
                direction='LEFT'
            if event.key==pygame.K_RIGHT and direction!='LEFT':
                direction='RIGHT'
    if direction=='UP':
        snake_position[1]-=10
    if direction=='DOWN':
        snake_position[1]+=10
    if direction=='LEFT':
        snake_position[0]-=10
    if direction=='RIGHT':
        snake_position[0]+=10
    snake.rect.center=snake_position
    scr.blit(snake.image, snake.rect)
    if not fruit_spawn:
        fruit_position=[random.randrange(1, (w//10)) * 10,random.randrange(1, (h//10)) * 10]
    fruit= pygame.sprite.Sprite()
    fruit.image= pygame.image.load("apple.png")
    fruit.rect = fruit.image.get_rect()
    fruit.rect.center=fruit_position
    scr.blit(fruit.image, fruit.rect)
    fruit_spawn = True
    def draw(snake, scr):
        scr.blit("body.jpg", snake.rect)
    if pygame.sprite.collide_mask(snake, fruit):
        score += 10
        fruit_spawn = False
        draw()
    if snake_position[0]<0 or snake_position[0]>w-13:
        game_over()
    if snake_position[1]<0 or snake_position[1]>h-13:
        game_over()
    show_score(white, 'Arial', 20)
    pygame.display.update()