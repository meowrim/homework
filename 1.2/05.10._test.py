import tkinter as tk
from typing import List
class Questions:
    def __init__(self, prompt: str, answers:List[str], correct_answers:List[int]):
        self.prompt=prompt
        self.answers=answers
        self.correct_answers=correct_answers
#Чтение из файла
def read_questions(filename: str) -> List[Questions]:
    with open(filename, "r", encoding="utf-8") as f:
        lines=f.readlines()
    list_questions=[]
    prompt=""
    answers=[]
    correct_answers=[]
    for line in lines:
        line=line.strip()
        if line.startswith("q:"):
            if prompt:
                list_questions.append(Questions(prompt, answers, correct_answers))
            prompt=line[2:]
            answers=[]
            correct_answers=[]
        elif line.startswith("+"):
            correct_answers.append(len(answers))
            answers.append(line[1:])
        else:
            answers.append(line)
    if prompt:
        list_questions.append(Questions(prompt, answers, correct_answers))
    return list_questions
class Test:
    def __init__(self, master, questions):
        self.master=master
        self.questions=questions
        self.question_number=0
        self.rez=0
        self.question_text=tk.Label(master, text="", font=("Arial", 16), wraplength=600)
        self.question_text.pack(pady=10)
        self.selected_answer=tk.IntVar()
        self.answer_buttons=[]
        frame=tk.Frame(master)
        frame.pack()
        for i in range(len(self.questions[0].answers)):
            button=tk.Radiobutton(frame, text="", variable=self.selected_answer, value=i)
            self.answer_buttons.append(button)
            button.grid(row=i, column=0, sticky="w", padx=5, pady=5)
        self.answer_button=tk.Button(master, text="Ответить", command=self.check_answer)
        self.answer_button.pack(pady=5)
        self.result_label=tk.Label(root, text="", font=("Arial", 12))
        self.result_label.pack(pady=20)
        self.rez_label=tk.Label(master, text="")
        self.rez_label.pack(pady=5)
        self.show_question()
        master.geometry("600x300")
#Показ вопросов
    def show_question(self):
        question=self.questions[self.question_number]
        self.question_text.configure(text=question.prompt)
        for i in range(len(question.answers)):
            self.answer_buttons[i].configure(text=question.answers[i])
#Проверка ответов
    def check_answer(self):
        question=self.questions[self.question_number]
        selected_answer=self.selected_answer.get()
        if selected_answer in question.correct_answers:
            self.rez+=1
            self.result_label.config(text="Правильно!")
        else:
            self.result_label.config(text="Неправильно!")
        self.question_number+=1
        if self.question_number==len(self.questions):
            self.show_result()
            self.answer_button.config(state="disabled")
        else:
            self.show_question()
#Показ результата
    def show_result(self):
        self.question_text.configure(text=f"Количество правильных ответов: {self.rez}/{len(self.questions)}")
        self.rez_label.configure(text=f"Процент правильных ответов: {self.rez / len(self.questions) * 100:.2f}%")
        self.result_label.config(text=" ")
if __name__ == "__main__":
    text_questions = read_questions("question.txt")
    root=tk.Tk()
    root.title("Тест по программированию")
    test=Test(root,text_questions)
    test.master.mainloop()
