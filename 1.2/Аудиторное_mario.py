import pygame

pygame.init()
scr=pygame.display.set_mode((700,37))
game_clock=pygame.time.Clock()
scr.fill((255,255,255))

m1=pygame.sprite.Sprite()
m1.image=pygame.image.load('animations/0.png')
m1.rect=m1.image.get_rect()
m1.rect.move_ip(30, 0)

m2=pygame.sprite.Sprite()
m2.image=pygame.image.load('animations/0.png')
m2.rect=m2.image.get_rect()
m2.rect.move_ip(650, 0)

mar_left=["animations/l1.png","animations/l2.png","animations/l3.png",
          "animations/l4.png","animations/l5.png"]
mar_right=["animations/r1.png","animations/r2.png","animations/r3.png",
          "animations/r4.png","animations/r5.png"]

running=True
while running:
    scr.fill((255,255,255))
    scr.blit(m1.image, m1.rect)
    scr.blit(m2.image, m2.rect)
    if pygame.sprite.collide_rect(m1, m2):
        quit()
    if pygame.key.get_pressed()[pygame.K_d]:
        for i in mar_right:
            m1.image = pygame.image.load(i)
            mar_right.remove(i)
            mar_right.append(i)
            m1.rect.move_ip(2, 0)
    if pygame.key.get_pressed()[pygame.K_a]:
        for i in mar_left:
            m1.image = pygame.image.load(i)
            mar_left.remove(i)
            mar_left.append(i)
            m1.rect.move_ip(-1, 0)
    if pygame.key.get_pressed()[pygame.K_RIGHT]:
        for i in mar_right:
            m2.image = pygame.image.load(i)
            mar_right.remove(i)
            mar_right.append(i)
            m2.rect.move_ip(1, 0)
    if pygame.key.get_pressed()[pygame.K_LEFT]:
        for i in mar_left:
            m2.image = pygame.image.load(i)
            mar_left.remove(i)
            mar_left.append(i)
            m2.rect.move_ip(-1, 0)
    if not pygame.key.get_pressed()[pygame.K_RIGHT] and not pygame.key.get_pressed()[pygame.K_LEFT]:
        m2.image=pygame.image.load("animations/0.png")
    if not pygame.key.get_pressed()[pygame.K_d] and not pygame.key.get_pressed()[pygame.K_a]:
        m1.image=pygame.image.load("animations/0.png")
    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            quit()
    pygame.display.flip()
    game_clock.tick(20)
