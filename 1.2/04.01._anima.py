import turtle
import random
scr=turtle.Screen()
border=turtle.Turtle()
border.width(5)
border.speed(0)#Моментальная отрисовка границ
border.up()#Чтобы не было линии от (0,0) до (200,200)
border.goto(200,200)
border.down()
border.goto(200,-200)
border.goto(-200,-200)
border.goto(-200,200)
border.goto(200,200)
border.hideturtle()

t=turtle.Turtle()
t.hideturtle()
t.shape("circle")
t.up()
randomx=random.randint(-180,180)
randomy=random.randint(-180,180)
t.goto(randomx,randomy)
t.showturtle()
shagx=1
shagy=1.5
while True:
    x,y=t.position() #Текущие координаты мячика
    if x+shagx>=190 or x+shagx<=-190:#Если досигло границы, то меняем на противоположную
        shagx=-shagx
    if y+shagy>=190 or y+shagy<=-190:
        shagy=-shagy
    t.goto(x+shagx,y+shagy)
