import turtle
turtle.setworldcoordinates(-3000,-3000,3000,3000)
turtle.speed(11)
turtle.color('slate blue')
def draw(x,y,tilt,radius):
    turtle.up()
    turtle.goto(x,y)
    turtle.down()
    turtle.seth(tilt-45)
    turtle.circle(radius)
    turtle.left(90)
    turtle.circle(radius)
for tilt in range(0,360,20):
    draw(0,0,tilt,1000)

